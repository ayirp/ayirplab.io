# ayirp.gitlab.io

Priya's personal website

## Serving this site locally

1.  Install [Ruby](https://www.ruby-lang.org/en/).
2.  Clone [this repository](https://gitlab.com/ayirp/ayirp.gitlab.io).
3.  Run the following to navigate to the website folder's directory and install all requirements:

    ```sh
    cd ayirp.gitlab.io
    gem install bundler
    bundle install
    ```

4.  Serve the site:

    ```sh
    bundle exec jekyll serve
    ```

5.  View the site at `http://localhost:4000`.

## Issues

If you get errors while trying to `bundle install`, some prerequisite libraries, such as `make` may be missing. Check the error messages and install the missing libraries to fix this issue.

To update gems:

```sh
bundle update
```

## License

Copyright (c) 2020-2022 Priya Streethran.
This site's source code is licensed under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Assets

- [Minimal Mistakes](https://mademistakes.com/work/minimal-mistakes-jekyll-theme/) - MIT License
- [Jekyll](https://jekyllrb.com/) - MIT License
- [GitLab](https://about.gitlab.com/) and [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) - MIT License
- [The Noun Project](https://thenounproject.com/) - [Creative Commons Attribution 3.0 United States (CC-BY-3.0-US) License](https://creativecommons.org/licenses/by/3.0/us/)
- [Font Awesome](https://fontawesome.com/), Version 5.15.3 - [SIL Open Font License 1.1 (OFL-1.1)](https://opensource.org/licenses/OFL-1.1) (fonts), MIT License (code), [Creative Commons Attribution 4.0 International (CC-BY-4.0) License](https://creativecommons.org/licenses/by/4.0/) (icons)
- [Susy](https://www.oddbird.net/susy/) - [BSD 3-clause "New" or "Revised" License (BSD-3-Clause)](https://opensource.org/licenses/BSD-3-Clause)
- [Breakpoint](http://breakpoint-sass.com/) - MIT License or [GNU General Public License, version 2 (GPL-2.0)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)
- [FitVids.js](https://github.com/davatron5000/FitVids.js/) - [WTFPL License](http://www.wtfpl.net/)
- [Magnific Popup](https://dimsemenov.com/plugins/magnific-popup/) - MIT License
- [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll) - MIT License
- [Gumshoejs](https://github.com/cferdinandi/gumshoe) - MIT License
- [jQuery throttle / debounce](https://benalman.com/projects/jquery-throttle-debounce-plugin/) - MIT License
- [GreedyNav.js](https://github.com/lukejacksonn/GreedyNav) - MIT License
- [Jekyll Group-By-Array](https://github.com/mushishi78/jekyll-group-by-array) - MIT License
- [@allejo's Pure Liquid Jekyll Table of Contents](https://allejo.io/blog/a-jekyll-toc-in-liquid-only/) - MIT License
- [Lunr](https://lunrjs.com) - MIT License
- [Rosé Pine base16 colour scheme](https://github.com/edunfelt/base16-rose-pine-scheme) - MIT License
- [Homemade Apple](https://fonts.google.com/specimen/Homemade+Apple) - [Apache License, Version 2.0 (Apache-2.0)](https://www.apache.org/licenses/LICENSE-2.0)
- [Staticman](https://staticman.net/) - MIT License
- [Heroku](https://www.heroku.com/)
- [Noto Emoji, v2017-05-18-cook-color-fix](https://github.com/googlefonts/noto-emoji/tree/v2017-05-18-cook-color-fix) - Apache-2.0
- [reCAPTCHA](https://www.google.com/recaptcha/about/)

## Privacy

All comments posted will be available publicly on [GitLab](https://gitlab.com/ayirp/ayirp.gitlab.io/-/tree/master/_data/comments).
An email address is required to post comments, but it will not be published.
reCAPTCHA is used to minimise spam.
