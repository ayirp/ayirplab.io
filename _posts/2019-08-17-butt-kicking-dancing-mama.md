---
title: The Butt Kicking, Dancing Mama
tags:
  - story
categories:
  - stories
last_modified_at: 2021-02-03
---

Few years after quitting my job and having children, I was depressed. As much as I loved motherhood, it was overwhelming and I started to miss my career, my friends, the freedom and everything that I had before I became a stay-at-home mom.

One day, a spur of the moment decision prompted me to take up Bharatanatyam with my daughter. I was the oldest, and the only married woman in my class at that time. It was very tough juggling family responsibilities and focusing on dance, especially since my husband works nearly 400 kilometres away from home.

I dragged my cranky Gremlins to all of my trainings, and finally after much blood, sweat and tears, I completed my Salanggai Pooja. I felt liberated and regained my self-confidence.

That started the ball rolling, and I also took up Taekwondo with my Gremlins.

Today, at almost 36, I'm a proud mother, a dancer and I have a 1st Dan black belt in Taekwondo. I have never felt better and I now have an insatiable desire for more things in life. It just goes to show that there is so much one can accomplish when you put your mind to it.

*✍️ Previously featured on [Malaysian Indian Anthology](https://www.facebook.com/malaysianindiananthology/photos/a.1086456218223783/1212183472317723) and [Astro Ulagam](https://www.astroulagam.com.my/community/depressed-mom-dancer-and-taekwondo-black-belt-114153)*
