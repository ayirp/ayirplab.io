---
title: The Strangers
tags:
  - story
categories:
  - stories
last_modified_at: 2021-02-03
---

At 22, forced to quit uni, her hopes and dreams of becoming a doctor was dashed. Married off to a man 10 years her senior, that she never knew existed. He was a distant cousin, so she was told. Days later, she was on a plane to Malaysia.

Her new home was a shophouse, as the family ran a restaurant and a couple of sundry shops. Although her husband was a kind man, she couldn't say the same about her in-laws. She was ill treated, but she kept mum, so did her husband, a filial son. But, at night, when she sobbed, he comforted her.

When the family business went bankrupt, they embarked on a new journey. He did all kinds of jobs. She gave tuition to the neighborhood kids. At night, after their children went to bed, she helped him study to get certified and he worked hard and ventured into the oil and gas industry, forever changing their lives.

Fast forward 38 years later, with three children and their spouses plus four grandchildren, they're still living a humble and contented life, setting an example to all. Those two people are my parents, an unlikely but perfect pair.
